Rails.application.routes.draw do
  resources :documents
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'users#index'
  resources :users

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

end
