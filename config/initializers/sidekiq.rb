# frozen_string_literal: true

# Sidekiq configuration(project specific) for redis server to use different db with namespace -> where 10 is the db name
Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://127.0.0.1:6379/10', namespace: 'rails5-active-storage-dev' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://127.0.0.1:6379/10', namespace: 'rails5-active-storage-dev' }
end