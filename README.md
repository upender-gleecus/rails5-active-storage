# README
A POC Rails 5.2.3 application for how to use **active-storage** including **webpacker**

ref: https://edgeguides.rubyonrails.org/active_storage_overview.html

>Create new rails application:

```ruby
rails new rails5-active-storage --webpack  --database=postgresql
rails db:create
```

and in other terminal window run webpack dev server

` ./bin/webpack-dev-server `

then 

**Install active-storage**
***
> it will generates migration to create tables - active_storage_blobs, active_storage_attachments
```ruby
   rails active_storage:install   
   rails db:migrate  
```   

> by default files uploaded Rails.root/storage folder
 you can change storage localtion or other settings in config/storage.yml
 

### Single file upload
***
>generate user scaffold

```ruby
 rails g scaffold User name:string age:integer
```` 

then add _avatar_ to user model
```ruby
 has_one_attached :avatar 
``` 

add _avatar_ to permitted params of users_controller

```ruby
 params.require(:user).permit(:name, :age, :avatar)
``` 

add below to views/users/_form.html.erb
```ruby
<%= form.label :avatar %>
<%= form.file_field :avatar %>
```
then add below to views/users/show.html for preview and download it(blob)

```ruby
<% if @user.avatar.attached? %>
  <p>
    <%= image_tag url_for(@user.avatar) %>
    <%= link_to "#{@user.avatar.filename} (download)", rails_blob_path(@user.avatar, disposition: "attachment") %>
  </p>
<% end %>
```
#### Transforming Images (variants/sizes)
***
 uncomment 'mini_magick' in Gemfile 
 
 To enable variants, add the image_processing gem to your Gemfile:
```ruby
  gem 'mini_magick', '~> 4.8'
  gem 'image_processing', '~> 1.2'
  bundle install
```

then in views 
```ruby
<%= image_tag url_for(@user.avatar.variant(resize: "100x100")) %>
```  

#### Rename file with secure random token
***
> add below code to User model to rename the filename of uploaded attachments
```ruby
 # models/user.rb
  
 before_validation :set_avatar_filename 
 
 private
 
 def set_avatar_filename
   avatar.blob.update(filename: secure_filename) if avatar.attached? # && avatar.new_record?
 end
 
 protected 
 
 def secure_filename
   var = :"@avatar_secure_token"
   token = instance_variable_get(var) || instance_variable_set(var, SecureRandom.uuid)
   "#{token}-#{Time.now.strftime('%d%m%Y%H%M%S%L-%12N')}"
 end 
```
#### Validating attachments
***
> present now 'active-storage' not providing any validations, to validate attachments add below gem file and update user model

> checkout list of validations at [active_storage_validations](https://github.com/igorkasyanchuk/active_storage_validations) 

```ruby
 gem 'active_storage_validations'
```
```ruby
 # models/users.rb
 
  validates :avatar, attached: true,
                      content_type: %w[image/png image/jpg image/jpeg],
                      size: { less_than: 3.megabytes, message: 'is not given between size' },
                      dimension: { width: { min: 100, max: 2400 },
                                   height: { min: 100, max: 1800 },
                                   message: 'is not given between dimension' } 

```


#### Setting sidekiq as active_job queue adapter
***
> active-storage uses 'active-job' to perform background jobs( purge/deleting attachments)

Add below to Gemfile and bundle install
```ruby
  gem 'sidekiq'
  gem 'redis-namespace' # different redis db with namespace for different rails projects, see below for more
  bundle install 
```
 config/application.rb 
```ruby 
  config.active_job.queue_adapter = :sidekiq
```  

config/initializers/sidekiq.rb -> create if not exists
```ruby 
  Sidekiq.configure_server do |config|
    config.redis = { url: 'redis://127.0.0.1:6379/10', namespace: 'rails5-active-storage-dev' }
  end
  
  Sidekiq.configure_client do |config|
    config.redis = { url: 'redis://127.0.0.1:6379/10', namespace: 'rails5-active-storage-dev' }
  end 
``` 
 config/routes.rb
```ruby 
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq' 
```   

run sidekiq to execute jobs and rails server(in different terminal tabs)
```ruby
  bundle exec sidekiq
  rails s
```

> Now click on any user edit and change avatar and submit , a job will be created to purge previously created blob attachment and sidekiq will executes the job successfully
 

### Multiple files upload
***
> Generate scaffold for Document model and multiple files as attachment
```ruby 
   rails g scaffold Document name:string
   rake db:migrate
```

update models/document.rb
```ruby
 # models/document.rb
 has_many_attached :files
 validates :name, presence: true 
```

permit the multiple files 
````ruby
# controllers/documents_controller.rb
params.require(:document).permit(:name, files: []) 
````

add below to views/documents/_form.html.erb
```ruby
  <div class="field">
     <%= form.label :files %>
     <%= form.file_field :files, multiple: true %>
  </div>
```

submit multiple files(images, audio , video , pdfs and other) and represent them  in /views/documents/show.html.erb like below


```ruby
<% if @document.files.attached? %>
  <div>
    <strong>Files:</strong>
    <% @document.files.each do |file| %>
      <p>
        <!-- previewable?:  Returns true if any registered previewer accepts the blob. By default, this will return true for videos and PDF documents -->
        <% if file.previewable? %>
          Thumb (<%= file.filename %>): <%= image_tag file.preview(resize: '100X100')  %> | &nbsp;
          <% if file.video? %>
            play Video:<%= video_tag rails_blob_path(file), controls: true, width: 400, height: 400 %>
          <% else  %>
            download :<%= link_to  "Download", rails_blob_path(file, disposition: 'attachment')%>
          <% end  %>
        <% elsif file.variable? %>
          <%= link_to image_tag(file.variant(resize: '100X100')), file , target: '_blank' %>
        <% elsif file.audio? %>
          play audio(<%= file.filename %>): <%= audio_tag(rails_blob_path(file), autoplay: false, controls: true) %>
        <% else %>
          <%= link_to  "Download file (#{file.filename})", rails_blob_path(file, disposition: 'attachment')%>
        <% end %>
    <% end %>
    </p>
  </div>
<% end %>
```
> Note: to understand previewable | representable check [documentation here](https://edgeapi.rubyonrails.org/classes/ActiveStorage/Blob/Representable.html#method-i-representable-3F)
1. ***previewable?***

     Returns true if any registered previewer accepts the blob. By default, this will return true for videos and PDF documents.
  
   ***preview***:  A preview is an image generated from a non-image blob
2. ***variable?*** 
    
    Returns true if ImageMagick can transform the blob
    
   ***variant***:  Returns an ActiveStorage::Variant instance with the set of transformations provided. This is only relevant for image files, and it allows any image to be transformed for size, colors, and the like
3. ***representable?***
 
   Returns true if the blob is variable or previewable.
   
   ***representation***: returns an ActiveStorage::Preview for a previewable blob or an ActiveStorage::Variant for a variable image blob.   


### Video/PDF uploads and preview
****
   ***video preview***
    
  to get the preview(thumb) from a uploaded video file install ***ffmpeg***
  
 ```ruby
  brew install ffmpeg
 ```
  
  in views
  
  ```ruby
  <% if file.previewable && file.video?  %>
     Video Thumb: <%= image_tag file.preview(resize: '100X100')  %> <br/>
     play Video: <%= video_tag rails_blob_path(file), controls: true, width: 400, height: 400 %>
  <% end %> 
  ```
  
   ***pdf preview***
   
  to get the preview(thumb) from a uploaded pdf file install ***mupdf***
  
   Go to the mupdf [downloads page](http://mupdf.com/downloads/) to find the latest version
   
  ```ruby
     brew install wget
     wget http://mupdf.com/downloads/archive/mupdf-1.15.0-source.tar.gz 
     tar -zxvf mupdf-1.15.0-source.tar.gz
     cd mupdf-1.15.0-source
     make prefix=/usr/local install
      
    # to test open any pdf file if preview is shown then installation is successful
     mupdf-gl some-pdf-file.pdf
  ``` 
  
  ****preview**** pdf in views 
  
  ```ruby
    <% if file.previewable? %>
      Thumb (<%= file.filename %>): <%= image_tag file.preview(resize: '200X200')  %> | 
      <%= link_to  "Download", rails_blob_path(file, disposition: 'attachment')%> 
    <% end %>
  ```
  
  ***Play audio***

  ```ruby
    <% if file.video?  %>
      play audio(<%= file.filename %>): <%= audio_tag(rails_blob_path(file), autoplay: false, controls: true) %>
    <% end %> 
  ```
  