# frozen_string_literal: true

# app/models/user.rb
class User < ApplicationRecord
  has_one_attached :avatar
  validates :name, presence: true
  validates :avatar, attached: true,
                     content_type: %w[image/png image/jpg image/jpeg],
                     size: { less_than: 3.megabytes, message: 'is not given between size' },
                     dimension: { width: { min: 100, max: 2400 },
                                  height: { min: 100, max: 1800 }, message: 'is not given between dimension' }

  before_validation :set_avatar_filename


  private

  # Set custom filename
  def set_avatar_filename
    avatar.blob.update(filename: secure_filename) if avatar.attached? && avatar.new_record?
  end


  protected

  def secure_filename
    var = :"@avatar_secure_token"
    token = instance_variable_get(var) || instance_variable_set(var, SecureRandom.uuid)
    "#{token}-#{Time.now.strftime('%d%m%Y%H%M%S%L-%12N')}"
  end


end
